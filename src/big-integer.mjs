/**
 * @source: https://www.npmjs.com/package/big-integer
 * @license magnet:?xt=urn:btih:5ac446d35272cc2e4e85e4325b146d0b7ca8f50c&dn=unlicense.txt Unlicense
 */

import bigInt from 'big-integer'
export { bigInt }

/* @license-end */
