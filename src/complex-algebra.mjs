/**
 * @source: https://www.npmjs.com/package/@kkitahara/complex-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { bigInt } from './big-integer.mjs'
import { ComplexAlgebraicElement } from './complex-algebraic-element.mjs'

/**
 * @desc
 * The ComplexAlgebra class is a class for complex algebra.
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
 * let ralg = new RealAlgebra()
 * let calg = new ComplexAlgebra(ralg)
 * let z, w, v
 *
 * // Generate a new real number
 * z = calg.num(1, 2, 5)
 * z.toString() // '(1 / 2)sqrt(5)'
 *
 * z = calg.num(1, 2)
 * z.toString() // '1 / 2'
 *
 * z = calg.num(3)
 * z.toString() // '3'
 *
 * // Generate a new imaginary number
 * z = calg.inum(1, 2, 5)
 * z.toString() // 'i((1 / 2)sqrt(5))'
 *
 * z = calg.inum(1, 2)
 * z.toString() // 'i(1 / 2)'
 *
 * z = calg.inum(3)
 * z.toString() // 'i(3)'
 *
 * // Real and imaginary parts
 * z = calg.num(1, 2, 5)
 * z.re.toString() // '(1 / 2)sqrt(5)'
 * z.im.toString() // '0'
 *
 * z = calg.inum(1, 2, 5)
 * z.re.toString() // '0'
 * z.im.toString() // '(1 / 2)sqrt(5)'
 *
 * // Generate from two real numbers (since v1.2.0)
 * let a = ralg.$(1, 2, 3)
 * let b = ralg.$(1, 2, 5)
 * z = calg.$(a, b)
 * z.toString() // '(1 / 2)sqrt(3) + i((1 / 2)sqrt(5))'
 *
 * // Copy (create a new object)
 * z = calg.inum(1, 2, 5)
 * w = calg.copy(z)
 * w.toString() // 'i((1 / 2)sqrt(5))'
 *
 * // Equality
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2, 5)
 * calg.eq(z, w) // false
 *
 * w = calg.num(1, 2, 5)
 * calg.eq(z, w) // true
 *
 * // Inequality
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2, 5)
 * calg.ne(z, w) // true
 *
 * w = calg.num(1, 2, 5)
 * calg.ne(z, w) // false
 *
 * // isZero
 * calg.isZero(calg.num(0)) // true
 * calg.isZero(calg.inum(0)) // true
 * calg.isZero(calg.num(1, 2, 5)) // false
 * calg.isZero(calg.inum(1, 2, 5)) // false
 * calg.isZero(calg.num(-1, 2, 5)) // false
 * calg.isZero(calg.inum(-1, 2, 5)) // false
 *
 * // isInteger (since v1.1.0)
 * calg.isInteger(calg.num(0)) // true
 * calg.isInteger(calg.inum(0)) // true
 * calg.isInteger(calg.num(1, 2)) // false
 * calg.isInteger(calg.inum(1, 2)) // false
 * calg.isInteger(calg.num(6, 3)) // true
 * calg.isInteger(calg.inum(6, 3)) // true
 * calg.isInteger(calg.num(6, 3, 2)) // false
 * calg.isInteger(calg.inum(6, 3, 2)) // false
 *
 * // Addition
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is generated
 * v = calg.add(z, w)
 * v.toString() // '(1 / 2)sqrt(5) + i(1 / 2)'
 *
 * // In-place addition
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is not generated
 * z = calg.iadd(z, w)
 * z.toString() // '(1 / 2)sqrt(5) + i(1 / 2)'
 *
 * // Subtraction
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is generated
 * v = calg.sub(z, w)
 * v.toString() // '(1 / 2)sqrt(5) + i(-1 / 2)'
 *
 * // In-place subtraction
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is not generated
 * z = calg.isub(z, w)
 * z.toString() // '(1 / 2)sqrt(5) + i(-1 / 2)'
 *
 * // Maltiplication
 * z = calg.inum(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is generated
 * v = calg.mul(z, w)
 * v.toString() // '-(1 / 4)sqrt(5)'
 *
 * // In-place multiplication
 * z = calg.inum(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is not generated
 * z = calg.imul(z, w)
 * z.toString() // '-(1 / 4)sqrt(5)'
 *
 * // Division
 * z = calg.inum(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is generated
 * v = calg.div(z, w)
 * v.toString() // 'sqrt(5)'
 *
 * // In-place division
 * z = calg.inum(1, 2, 5)
 * w = calg.inum(1, 2)
 * // new object is not generated
 * z = calg.idiv(z, w)
 * z.toString() // 'sqrt(5)'
 *
 * // Multiplication by -1
 * z = calg.inum(1, 2, 5)
 * // new object is generated
 * w = calg.neg(z)
 * w.toString() // 'i(-(1 / 2)sqrt(5))'
 *
 * // In-place multiplication by -1
 * z = calg.inum(1, 2, 5)
 * // new object is not generated
 * z = calg.ineg(z)
 * z.toString() // 'i(-(1 / 2)sqrt(5))'
 *
 * // Complex conjugate
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2, 5)
 * // new object is generated
 * v = calg.cjg(z)
 * v.toString() // '(1 / 2)sqrt(5)'
 * v = calg.cjg(w)
 * v.toString() // 'i(-(1 / 2)sqrt(5))'
 *
 * // In-place evaluation of the complex conjugate
 * z = calg.num(1, 2, 5)
 * w = calg.inum(1, 2, 5)
 * // new object is not generated
 * z = calg.icjg(z)
 * z.toString() // '(1 / 2)sqrt(5)'
 * w = calg.icjg(w)
 * w.toString() // 'i(-(1 / 2)sqrt(5))'
 *
 * // Square of the absolute value
 * z = calg.iadd(calg.num(3), calg.inum(4))
 * a = calg.abs2(z)
 * a.toString() // '25'
 * // return value is not a complex number (but a real number)
 * a.re // undefined
 * a.im // undefined
 *
 * // JSON (stringify and parse)
 * z = calg.iadd(calg.num(1, 2, 5), calg.inum(-1, 2, 7))
 * let str = JSON.stringify(z)
 * w = JSON.parse(str, calg.reviver)
 * calg.eq(z, w) // true
 */
export class ComplexAlgebra {
  /**
   * @desc
   * The constructor of the {@link ComplexAlgebra} class.
   *
   * CAUTION: this function does not check if `ralg` is a valid implementation
   * of {@link RealAlgebra}.
   *
   * @param {RealAlgebra} ralg
   * an instance of {@link RealAlgebra}.
   *
   * @version 1.2.2
   * @since 1.0.0
   */
  constructor (ralg) {
    /**
     * @desc
     * The ComplexAlgebra#ralg is used
     * to manipurate real or imaginary part of
     * {@link ComplexAlgebraicElement}s.
     *
     * @type {RealAlgebra}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.ralg = ralg
  }

  /**
   * @desc
   * The ComplexAlgebra#$ method returns a new instance of
   * {@link ComplexAlgebraicElement} representing `a + bj`.
   * It uses copies of `a` and `b`.
   *
   * @param {RealAlgebraicElement} [a = 0]
   * real part.
   *
   * @param {RealAlgebraicElement} [b = 0]
   * imaginary part.
   *
   * @return {ComplexAlgebraicElement}
   * a {@link ComplexAlgebraicElement} representing
   * `a + bj`
   *
   * @version 1.2.0
   * @since 1.2.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.$(1, 2)
   * z instanceof C // true
   * ralg.eq(z.re, 1) // true
   * ralg.eq(z.im, 2) // true
   *
   * z = calg.$()
   * z instanceof C // true
   * ralg.eq(z.re, 0) // true
   * ralg.eq(z.im, 0) // true
   */
  $ (a = 0, b = 0) {
    const ralg = this.ralg
    a = ralg.copy(a)
    b = ralg.copy(b)
    return new ComplexAlgebraicElement(a, b)
  }

  /**
   * @desc
   * The ComplexAlgebra#num method returns
   * a {@link ComplexAlgebraicElement} representing
   * (*p* / *q*)sqrt(*b*) if the 3rd parameter is positive (or zero)
   * and i(*p* / *q*)sqrt(-*b*) otherwise,
   * where *p* is an integer, *q* is a non-zero integer,
   * and the absolute value of *b* is a square-free integer.
   *
   * CAUTION: this method does not check if the absolute value of `b`
   * is square-free.
   *
   * @param {number|bigInt} [p = 0]
   * an integer.
   *
   * @param {number|bigInt} [q = 1]
   * a non-zero integer (can be negative).
   *
   * @param {number|bigInt} [b = 1]
   * a square-free integer.
   *
   * @return {ComplexAlgebraicElement}
   * a {@link ComplexAlgebraicElement} representing
   * `(p / q)sqrt(b)` if `b` is positive (or zero)
   * and `i(p / q)sqrt(-b)` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * import bigInt from 'big-integer'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let a = ralg.num(1, 2, 5)
   * let b = ralg.num(0, 2, 5)
   * let z = calg.num(1, 2, 5)
   * let w = calg.num(1, 2, -5)
   *
   * z instanceof C // true
   * ralg.eq(z.re, a) // true
   * ralg.eq(z.im, b) // true
   *
   * w instanceof C // true
   * ralg.eq(w.re, b) // true
   * ralg.eq(w.im, a) // true
   *
   * w = calg.num(1, 2, bigInt(-5))
   * w instanceof C // true
   * ralg.eq(w.re, b) // true
   * ralg.eq(w.im, a) // true
   *
   * w = calg.num()
   * w instanceof C // true
   * ralg.eq(w.re, 0) // true
   * ralg.eq(w.im, 0) // true
   */
  num (p = 0, q = 1, b = 1) {
    const ralg = this.ralg
    if (ralg.isZero(b) || ralg.isPositive(b)) {
      return new ComplexAlgebraicElement(ralg.num(p, q, b), ralg.num(0))
    } else {
      if (bigInt.isInstance(b)) {
        b = b.times(-1)
      } else {
        b = -b
      }
      return new ComplexAlgebraicElement(ralg.num(0), ralg.num(p, q, b))
    }
  }

  /**
   * @desc
   * The ComplexAlgebra#inum method returns
   * a {@link ComplexAlgebraicElement} representing
   * i(*p* / *q*)sqrt(*b*) if the 3rd parameter is positive (or zero)
   * and -(*p* / *q*)sqrt(-*b*) otherwise,
   * where *p* is an integer, *q* is a non-zero integer,
   * and the absolute value of *b* is a square-free integer.
   *
   * CAUTION: this method does not check if the absolute value of `b`
   * is square-free.
   *
   * @param {number} [p = 0]
   * an integer.
   *
   * @param {number} [q = 1]
   * a non-zero integer (can be negative).
   *
   * @param {number} [b = 1]
   * a square-free integer.
   *
   * @return {ComplexAlgebraicElement}
   * a {@link ComplexAlgebraicElement} representing
   * `i(p / q)sqrt(b)` if `b` is positive (or zero)
   * and `-(p / q)sqrt(-b)` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * import bigInt from 'big-integer'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let a = ralg.num(1, 2, 5)
   * let b = ralg.num(-1, 2, 5)
   * let c = ralg.num(0, 2, 5)
   * let z = calg.inum(1, 2, 5)
   * let w = calg.inum(1, 2, -5)
   *
   * z instanceof C // true
   * ralg.eq(z.re, c) // true
   * ralg.eq(z.im, a) // true
   *
   * w instanceof C // true
   * ralg.eq(w.re, b) // true
   * ralg.eq(w.im, c) // true
   *
   * w = calg.inum(bigInt(1), 2, bigInt(-5))
   * w instanceof C // true
   * ralg.eq(w.re, b) // true
   * ralg.eq(w.im, c) // true
   *
   * w = calg.inum()
   * w instanceof C // true
   * ralg.eq(w.re, 0) // true
   * ralg.eq(w.im, 0) // true
   */
  inum (p = 0, q = 1, b = 1) {
    const ralg = this.ralg
    if (ralg.isZero(b) || ralg.isPositive(b)) {
      return new ComplexAlgebraicElement(ralg.num(0), ralg.num(p, q, b))
    } else {
      if (bigInt.isInstance(b)) {
        b = b.times(-1)
      } else {
        b = -b
      }
      if (bigInt.isInstance(p)) {
        p = p.times(-1)
      } else {
        p = -p
      }
      return new ComplexAlgebraicElement(ralg.num(p, q, b), ralg.num(0))
    }
  }

  /**
   * @desc
   * The ComplexAlgebra#cast method just returns
   * `z` if `z` is a {@link ComplexAlgebraicElement}
   * and both `z.re` and `z.im` are valid instances of
   * {@link RealAlgebraicElement},
   * and otherwise casts `z` to a {@link ComplexAlgebraicElement}.
   *
   * @param {object} z
   * an object.
   *
   * @return {ComplexAlgebraicElement}
   * `z` or a new {@link ComplexAlgebraicElement}.
   *
   * @version 1.2.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = z
   *
   * w === calg.cast(z) // true
   * calg.cast(1) instanceof C // true
   * w === calg.cast(1) // false
   * calg.eq(z, calg.cast(1)) // true
   *
   * calg.eq(calg.cast({ re: 2, im: 1 }), new C(2, 1)) // true
   *
   * z = new C(1, 2)
   * ralg.cast(z.re) === z.re // false
   * w = calg.cast(z)
   * w === z // false
   * ralg.cast(w.re) === w.re // true
   */
  cast (z) {
    if (!(z instanceof ComplexAlgebraicElement)) {
      const ralg = this.ralg
      const re = z.re
      const im = z.im
      if (re !== undefined || im !== undefined) {
        z = new ComplexAlgebraicElement(ralg.cast(re), ralg.cast(im))
      } else {
        z = new ComplexAlgebraicElement(ralg.cast(z), ralg.$(0))
      }
    } else {
      const ralg = this.ralg
      const re = ralg.cast(z.re)
      const im = ralg.cast(z.im)
      if (re !== z.re || im !== z.im) {
        z = new z.constructor(re, im)
      }
    }
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#copy method returns a copy of `z`.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a copy of `z`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = calg.copy(z)
   *
   * z instanceof C // true
   * w instanceof C // true
   * ralg.eq(z.re, w.re) // true
   * ralg.eq(z.im, w.im) // true
   * z !== w // true
   * z.re !== w.re // true
   * z.im !== w.im // true
   */
  copy (z) {
    z = this.cast(z)
    const ralg = this.ralg
    return new z.constructor(ralg.copy(z.re), ralg.copy(z.im))
  }

  /**
   * @desc
   * The ComplexAlgebra#eq method checks if `z` is equal to `w`.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `z` is equal to `w` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1, 2, -5)
   *
   * calg.eq(z, calg.num(1, 2, -5)) // true
   * calg.eq(z, calg.num(1, 2, 5)) // false
   * calg.eq(z, calg.num(-1, 2, -5)) // false
   */
  eq (z, w) {
    z = this.cast(z)
    w = this.cast(w)
    const ralg = this.ralg
    return ralg.eq(z.re, w.re) && ralg.eq(z.im, w.im)
  }

  /**
   * @desc
   * The ComplexAlgebra#ne method checks if `z` is not equal to `w`.
   *
   * `calg.ne(z, w)` is an alias for `!calg.eq(z, w)`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `z` is not equal to `w` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1, 2, -5)
   *
   * calg.ne(z, calg.num(1, 2, -5)) // false
   * calg.ne(z, calg.num(1, 2, 5)) // true
   * calg.ne(z, calg.num(-1, 2, -5)) // true
   */
  ne (z, w) {
    return !this.eq(z, w)
  }

  /**
   * @desc
   * The ComplexAlgebra#isZero method checks if `z` is zero.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `z` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * calg.isZero(calg.num(0, 2, 5)) // true
   * calg.isZero(calg.num(1, 2, 5)) // false
   * calg.isZero(calg.num(1, 2, -5)) // false
   */
  isZero (z) {
    z = this.cast(z)
    const ralg = this.ralg
    return ralg.isZero(z.re) && ralg.isZero(z.im)
  }

  /**
   * @desc
   * The ComplexAlgebra#isInteger method checks if both
   * the real and imaginary parts of `z` are integers.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if both the real and imaginary parts of `z` are integers
   * and `false` otherwise.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * calg.isInteger(calg.num(0, 2, 5)) // true
   * calg.isInteger(calg.num(1, 2, 5)) // false
   * calg.isInteger(calg.inum(1, 2, 5)) // false
   * calg.isInteger(calg.num(4, 2, 1)) // true
   * calg.isInteger(calg.inum(4, 2, 1)) // true
   */
  isInteger (z) {
    z = this.cast(z)
    const ralg = this.ralg
    return ralg.isInteger(z.re) && ralg.isInteger(z.im)
  }

  /**
   * @desc
   * The ComplexAlgebra#isFinite method checks if `z` is finite.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `z` is finite and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * import bigInt from 'big-integer'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let a = bigInt('1e999')
   * calg.isFinite(calg.num(a, 2, 5)) // true
   * calg.isFinite(calg.num(a, 2, -5)) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let a = 1e999
   * calg.isFinite(calg.num(1, 2, 5)) // true
   * calg.isFinite(calg.num(a, 2, -5)) // false
   */
  isFinite (z) {
    z = this.cast(z)
    const ralg = this.ralg
    return ralg.isFinite(z.re) && ralg.isFinite(z.im)
  }

  /**
   * @desc
   * The ComplexAlgebra#isExact method checks
   * if `this` is an implementation of exact algebra.
   *
   * @return {boolean}
   * `true` if `this` is an exact algebra and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * calg.isExact() // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * calg.isExact() // false
   */
  isExact () {
    return this.ralg.isExact()
  }

  /**
   * @desc
   * The ComplexAlgebra#isReal method checks
   * if `this` is an implementation of real algebra.
   *
   * @return {boolean}
   * `false`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * ralg.isReal() // true
   * calg.isReal() // false
   */
  isReal () {
    return false
  }

  /**
   * @desc
   * The ComplexAlgebra#add method returns the result of
   * the addition `z` plus `w`.
   *
   * `calg.add(z, w)` is an alias for `calg.iadd(calg.copy(z), w)`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the result of the addition `z` plus `w`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = calg.num(-1, 1, -1)
   *
   * let v = calg.add(z, w)
   * v instanceof C // true
   * v !== z // true
   * calg.eq(z, new C(1, 0)) // true
   * calg.eq(v, new C(1, -1)) // true
   */
  add (z, w) {
    return this.iadd(this.copy(z), w)
  }

  /**
   * @desc
   * The ComplexAlgebra#iadd method adds `w` to `z` *in place*.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = calg.num(-1, 1, -1)
   * let v = z
   *
   * // GOOD-PRACTICE!
   * z = calg.iadd(z, w)
   * z === v // true
   * z instanceof C // true
   * calg.eq(z, new C(1, -1)) // true
   */
  iadd (z, w) {
    z = this.cast(z)
    w = this.cast(w)
    const ralg = this.ralg
    z.re = ralg.iadd(z.re, w.re)
    z.im = ralg.iadd(z.im, w.im)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#sub method returns the result of
   * the subtraction `z` minus `w`.
   *
   * `calg.sub(z, w)` is an alias for `calg.isub(calg.copy(z), w)`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the result of the subtraction `z` minus `w`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = calg.num(-1, 1, -1)
   *
   * let v = calg.sub(z, w)
   * v instanceof C // true
   * v !== z // true
   * calg.eq(z, new C(1, 0)) // true
   * calg.eq(v, new C(1, 1)) // true
   */
  sub (z, w) {
    return this.isub(this.copy(z), w)
  }

  /**
   * @desc
   * The ComplexAlgebra#isub method subtracts `w` from `z` *in place*.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1)
   * let w = calg.num(-1, 1, -1)
   * let v = z
   *
   * // GOOD-PRACTICE!
   * z = calg.isub(z, w)
   * z === v // true
   * z instanceof C // true
   * calg.eq(z, new C(1, 1)) // true
   */
  isub (z, w) {
    z = this.cast(z)
    w = this.cast(w)
    const ralg = this.ralg
    z.re = ralg.isub(z.re, w.re)
    z.im = ralg.isub(z.im, w.im)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#mul method returns the result of
   * the multiplication `z` times `w`.
   *
   * `calg.mul(z, w)` is an alias for `calg.imul(calg.copy(z), w)`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the result of the multiplication `z` times `w`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1, 2, 5)
   * let w = calg.num(-1, 2, -5)
   *
   * let v = calg.mul(z, w)
   * v instanceof C // true
   * v !== z // true
   * calg.eq(z, new C(ralg.num(1, 2, 5), 0)) // true
   * calg.eq(v, new C(0, ralg.num(-5, 4))) // true
   */
  mul (z, w) {
    return this.imul(this.copy(z), w)
  }

  /**
   * @desc
   * The ComplexAlgebra#imul method multiplies `z` by `w` *in place*.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.num(1, 2, 5)
   * let w = calg.num(-1, 2, -5)
   * let v = z
   *
   * // GOOD-PRACTICE!
   * z = calg.imul(z, w)
   * z === v // true
   * z instanceof C // true
   * calg.eq(z, new C(0, ralg.num(-5, 4))) // true
   */
  imul (z, w) {
    z = this.cast(z)
    w = this.cast(w)
    const ralg = this.ralg
    const d = w.im
    const c = w.re
    const bd = ralg.mul(z.im, d)
    const ad = ralg.mul(z.re, d)
    z.re = ralg.imul(z.re, c)
    z.re = ralg.isub(z.re, bd)
    z.im = ralg.imul(z.im, c)
    z.im = ralg.iadd(z.im, ad)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#div method returns the result of
   * the division `z` over `w`.
   *
   * `calg.div(z, w)` is an alias for `calg.idiv(calg.copy(z), w)`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the result of the division `z` over `w`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   * let w = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * let v = calg.div(z, w)
   * v instanceof C // true
   * v !== z // true
   * calg.eq(z, new C(1, -1)) // true
   * calg.eq(v, new C(1, 0)) // true
   */
  div (z, w) {
    return this.idiv(this.copy(z), w)
  }

  /**
   * @desc
   * The ComplexAlgebra#idiv method divides `z` by `w` *in place*.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @param {ComplexAlgebraicElement} w
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   * let w = calg.iadd(calg.num(1), calg.inum(-1))
   * let v = z
   *
   * // GOOD-PRACTICE!
   * z = calg.idiv(z, w)
   * z === v // true
   * z instanceof C // true
   * calg.eq(z, new C(1, 0)) // true
   */
  idiv (z, w) {
    z = this.cast(z)
    w = this.cast(w)
    const abs2w = this.abs2(w)
    const ralg = this.ralg
    const d = w.im
    const c = w.re
    const bd = ralg.mul(z.im, d)
    const ad = ralg.mul(z.re, d)
    z.re = ralg.imul(z.re, c)
    z.re = ralg.iadd(z.re, bd)
    z.re = ralg.idiv(z.re, abs2w)
    z.im = ralg.imul(z.im, c)
    z.im = ralg.isub(z.im, ad)
    z.im = ralg.idiv(z.im, abs2w)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#neg method returns the result of
   * the multiplication `z` times `-1`.
   *
   * `calg.neg(z)` is an alias for `calg.ineg(calg.copy(z))`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the result of the multiplication `z` times `-1`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * let w = calg.neg(z)
   * w instanceof C // true
   * w !== z // true
   * calg.eq(z, new C(1, -1)) // true
   * calg.eq(w, new C(-1, 1)) // true
   */
  neg (z) {
    return this.ineg(this.copy(z))
  }

  /**
   * @desc
   * The ComplexAlgebra#ineg method multiplies `z` by `-1` *in place*.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   * let w = z
   *
   * // GOOD-PRACTICE!
   * z = calg.ineg(z)
   * z === w // true
   * z instanceof C // true
   * calg.eq(z, new C(-1, 1)) // true
   */
  ineg (z) {
    z = this.cast(z)
    const ralg = this.ralg
    z.re = ralg.ineg(z.re)
    z.im = ralg.ineg(z.im)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#cjg method returns the complex conjugate of `z`.
   *
   * `calg.cjg(z)` is an alias for `calg.icjg(calg.copy(z))`,
   * where `calg` is an instance of {@link ComplexAlgebra}.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * a new {@link ComplexAlgebraicElement}
   * representing the complex conjugate of `z`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * let w = calg.cjg(z)
   * w instanceof C // true
   * w !== z // true
   * calg.eq(z, new C(1, -1)) // true
   * calg.eq(w, new C(1, 1)) // true
   */
  cjg (z) {
    return this.icjg(this.copy(z))
  }

  /**
   * @desc
   * The ComplexAlgebra#icjg method
   * evaluates the complex conjugate of `z` and stores the result to `z`.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {ComplexAlgebraicElement}
   * `z`, or a new {@link ComplexAlgebraicElement} if `z` is not a
   * {@link ComplexAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   * let w = z
   *
   * // GOOD-PRACTICE!
   * z = calg.icjg(z)
   * z === w // true
   * z instanceof C // true
   * calg.eq(z, new C(1, 1)) // true
   */
  icjg (z) {
    z = this.cast(z)
    z.im = this.ralg.ineg(z.im)
    return z
  }

  /**
   * @desc
   * The ComplexAlgebra#abs method returns the absolute value of `z`.
   *
   * CAUTION: this method is not implemented for exact algebra.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the absolute value of `z`.
   *
   * @throws {Error}
   * if `this` is an implementation of exact algebra.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * calg.abs(z) // Error
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * let w = calg.abs(z)
   * w instanceof C // false
   * typeof w === 'number' // true
   * calg.eq(z, new C(1, -1)) // true
   * ralg.eq(w, Math.sqrt(2)) // true
   */
  abs (z) {
    if (this.isExact()) {
      throw Error('`abs` is not implemented for exact algebra.')
    } else {
      return Math.sqrt(this.abs2(z))
    }
  }

  /**
   * @desc
   * The ComplexAlgebra#abs2 method returns
   * the square of the absolute value of `z`.
   *
   * @param {ComplexAlgebraicElement} z
   * a {@link ComplexAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement}
   * representing the square of the absolute value of `z`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C, ComplexAlgebra }
   *   from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1), calg.inum(-1))
   *
   * let w = calg.abs2(z)
   * w instanceof C // false
   * w instanceof P // true
   * calg.eq(z, new C(1, -1)) // true
   * ralg.eq(w, ralg.num(2)) // true
   */
  abs2 (z) {
    z = this.cast(z)
    const ralg = this.ralg
    return ralg.iadd(ralg.abs2(z.re), ralg.abs2(z.im))
  }

  /**
   * @desc
   * The reviver function for the {@link ComplexAlgebraicElement}s.
   *
   * @type {Function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebra } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   * let calg = new ComplexAlgebra(ralg)
   *
   * let z = calg.iadd(calg.num(1, 2, 5), calg.inum(-1, 2, 7))
   * let str = JSON.stringify(z)
   * let w = JSON.parse(str, calg.reviver)
   * calg.eq(z, w) // true
   */
  get reviver () {
    const ralg = this.ralg
    return function (key, value) {
      value = ralg.reviver(key, value)
      value = ComplexAlgebraicElement.reviver(key, value)
      return value
    }
  }
}

/* @license-end */
