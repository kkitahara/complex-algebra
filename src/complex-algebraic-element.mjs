/**
 * @source: https://www.npmjs.com/package/@kkitahara/complex-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {number|Polynomial} RealAlgebraicElement
 *
 * @desc
 * A RealAlgebraicElement denotes a {@link number}
 * or a {@link Polynomial} as follows.
 * * A {@link number} for the numerical algebra.
 * * A {@link Polynomial} for the exact algebra.
 *
 * See the documents of @kkitahara/real-algebra for details.
 */

/**
 * @desc
 * The ComplexAlgebraicElement class is a class for complex algebraic elements.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class ComplexAlgebraicElement {
  /**
   * @desc
   * The constructor function of the {@link ComplexAlgebraicElement} class.
   *
   * CAUTION: this constructor function does not check if `a` and `b`
   * are valid {@link RealAlgebraicElement}s.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ComplexAlgebraicElement as C } from '@kkitahara/complex-algebra'
   *
   * let a = new C(1, 2)
   * a.re === 1 // true
   * a.im === 2 // true
   */
  constructor (a, b) {
    /**
     * @desc
     * The ComplexAlgebraicElement#re
     * is the real part of the {@link ComplexAlgebraicElement}.
     *
     * @type {RealAlgebraicElement}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.re = a
    /**
     * @desc
     * The ComplexAlgebraicElement#im
     * is the imaginary part of the {@link ComplexAlgebraicElement}.
     *
     * @type {RealAlgebraicElement}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.im = b
  }

  /**
   * @desc
   * The ComplexAlgebraicElement#toString method converts
   * `this` to a human-readable {@link string}.
   *
   * @param {number} [radix]
   * the base to use for representing numeric values.
   *
   * @return {string}
   * a human-readable {@link string} representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   *
   * new C(1, 0).toString() // '1'
   * new C(0, -1).toString() // 'i(-1)'
   * new C(-1, -1).toString() // '-1 + i(-1)'
   *
   * let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
   * let b = ralg.iadd(ralg.num(-1), a)
   * let z = new C(a, b)
   * z.toString() // '-1 / 2 - (1 / 2)sqrt(5) + i(-3 / 2 - (1 / 2)sqrt(5))'
   *
   * new C(-3, -5).toString(2) // '-11 + i(-101)'
   *
   * new C(0, 0).toString(2) // '0'
   */
  toString (radix) {
    let s = ''
    if (typeof this.re === 'number') {
      if (this.re !== 0) {
        s += this.re.toString(radix)
      }
    } else if (!this.re.isZero()) {
      s += this.re.toString(radix)
    }
    if (typeof this.im === 'number') {
      if (this.im !== 0) {
        if (s.length === 0) {
          s += 'i(' + this.im.toString(radix) + ')'
        } else {
          s += ' + i(' + this.im.toString(radix) + ')'
        }
      }
    } else if (!this.im.isZero()) {
      if (s.length === 0) {
        s += 'i(' + this.im.toString(radix) + ')'
      } else {
        s += ' + i(' + this.im.toString(radix) + ')'
      }
    }
    if (s.length === 0) {
      s = '0'
    }
    return s
  }

  /**
   * @desc
   * The ComplexAlgebraicElement#toFixed method returns
   * the fixed-point representation of `this`.
   *
   * @param {number} [digits]
   * the number of digits appear after the decimal point.
   *
   * @return {string}
   * the fixed-point representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
   * let b = ralg.iadd(ralg.num(-1), a)
   * let z = new C(a, b)
   * z.toFixed(3) // '-1.618 + i(-2.618)'
   *
   * z = new C(0, b)
   * z.toFixed(3) // 'i(-2.618)'
   *
   * z = new C(ralg.num(0), b)
   * z.toFixed(3) // 'i(-2.618)'
   *
   * z = new C(b, ralg.num(0))
   * z.toFixed(3) // '-2.618'
   *
   * z = new C(1.3, 3.2)
   * z.toFixed(3) // '1.300 + i(3.200)'
   *
   * z = new C(0, 3.2)
   * z.toFixed(3) // 'i(3.200)'
   *
   * z = new C(0, 0)
   * z.toFixed(3) // '0.000'
   */
  toFixed (digits) {
    let s = ''
    if (typeof this.re === 'number') {
      if (this.re !== 0) {
        s = s + this.re.toFixed(digits)
      }
    } else if (!this.re.isZero()) {
      s = s + this.re.toFixed(digits)
    }
    if (typeof this.im === 'number') {
      if (this.im !== 0) {
        if (s.length === 0) {
          s = s + 'i(' + this.im.toFixed(digits) + ')'
        } else {
          s = s + ' + i(' + this.im.toFixed(digits) + ')'
        }
      }
    } else if (!this.im.isZero()) {
      if (s.length === 0) {
        s = s + 'i(' + this.im.toFixed(digits) + ')'
      } else {
        s = s + ' + i(' + this.im.toFixed(digits) + ')'
      }
    }
    if (s.length === 0) {
      s = this.re.toFixed(digits)
    }
    return s
  }

  /**
   * @desc
   * The ComplexAlgebraicElement#toJSON method converts
   * `this` to an object serialisable by `JSON.stringify`.
   *
   * @return {object}
   * a serialisable object for `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * import { ComplexAlgebraicElement as C } from '@kkitahara/complex-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
   * let b = ralg.iadd(ralg.num(-1), a)
   * let z = new C(a, b)
   *
   * // toJSON method is called by JSON.stringify
   * let s = JSON.stringify(z)
   *
   * typeof s // 'string'
   */
  toJSON () {
    const obj = {}
    obj.reviver = 'Complex'
    obj.version = '1.0.0'
    obj.re = this.re
    obj.im = this.im
    return obj
  }

  /**
   * @desc
   * The ComplexAlgebraicElement.reviver function converts
   * the data produced by {@link ComplexAlgebraicElement#toJSON}
   * to a {@link ComplexAlgebraicElement}.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {ComplexAlgebraicElement|object}
   * a {@link ComplexAlgebraicElement} or `value`.
   *
   * @throws {Error}
   * if the given object is invalid.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { ComplexAlgebraicElement as C } from '@kkitahara/complex-algebra'
   *
   * let a = 1 / 2 * Math.sqrt(5)
   * let b = a - 1
   * let z = new C(a, b)
   * let s = JSON.stringify(z)
   *
   * let w = JSON.parse(s, C.reviver)
   *
   * typeof s // 'string'
   * w instanceof C // true
   * w !== z // true
   * z.re === w.re // true
   * z.im === w.im // true
   *
   * let s2 = s.replace('1.0.0', '0.0.0')
   * JSON.parse(s2, C.reviver) // Error
   */
  static reviver (key, value) {
    if (value !== null && typeof value === 'object' &&
        value.reviver === 'Complex') {
      if (value.version === '1.0.0') {
        return new ComplexAlgebraicElement(value.re, value.im)
      } else {
        throw Error('invalid version.')
      }
    } else {
      return value
    }
  }
}

/* @license-end */
