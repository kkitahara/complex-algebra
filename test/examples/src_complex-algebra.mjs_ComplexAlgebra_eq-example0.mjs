import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1, 2, -5)

testDriver.test(() => { return calg.eq(z, calg.num(1, 2, -5)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#eq-example0_0', false)
testDriver.test(() => { return calg.eq(z, calg.num(1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#eq-example0_1', false)
testDriver.test(() => { return calg.eq(z, calg.num(-1, 2, -5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#eq-example0_2', false)
