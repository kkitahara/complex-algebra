import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
let b = ralg.iadd(ralg.num(-1), a)
let z = new C(a, b)

// toJSON method is called by JSON.stringify
let s = JSON.stringify(z)

testDriver.test(() => { return typeof s }, 'string', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toJSON-example0_0', false)
