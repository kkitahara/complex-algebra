import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)
let z, w, v

// Generate a new real number
z = calg.num(1, 2, 5)
testDriver.test(() => { return z.toString() }, '(1 / 2)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_0', false)

z = calg.num(1, 2)
testDriver.test(() => { return z.toString() }, '1 / 2', 'src/complex-algebra.mjs~ComplexAlgebra-example0_1', false)

z = calg.num(3)
testDriver.test(() => { return z.toString() }, '3', 'src/complex-algebra.mjs~ComplexAlgebra-example0_2', false)

// Generate a new imaginary number
z = calg.inum(1, 2, 5)
testDriver.test(() => { return z.toString() }, 'i((1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_3', false)

z = calg.inum(1, 2)
testDriver.test(() => { return z.toString() }, 'i(1 / 2)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_4', false)

z = calg.inum(3)
testDriver.test(() => { return z.toString() }, 'i(3)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_5', false)

// Real and imaginary parts
z = calg.num(1, 2, 5)
testDriver.test(() => { return z.re.toString() }, '(1 / 2)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_6', false)
testDriver.test(() => { return z.im.toString() }, '0', 'src/complex-algebra.mjs~ComplexAlgebra-example0_7', false)

z = calg.inum(1, 2, 5)
testDriver.test(() => { return z.re.toString() }, '0', 'src/complex-algebra.mjs~ComplexAlgebra-example0_8', false)
testDriver.test(() => { return z.im.toString() }, '(1 / 2)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_9', false)

// Generate from two real numbers (since v1.2.0)
let a = ralg.$(1, 2, 3)
let b = ralg.$(1, 2, 5)
z = calg.$(a, b)
testDriver.test(() => { return z.toString() }, '(1 / 2)sqrt(3) + i((1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_10', false)

// Copy (create a new object)
z = calg.inum(1, 2, 5)
w = calg.copy(z)
testDriver.test(() => { return w.toString() }, 'i((1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_11', false)

// Equality
z = calg.num(1, 2, 5)
w = calg.inum(1, 2, 5)
testDriver.test(() => { return calg.eq(z, w) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_12', false)

w = calg.num(1, 2, 5)
testDriver.test(() => { return calg.eq(z, w) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_13', false)

// Inequality
z = calg.num(1, 2, 5)
w = calg.inum(1, 2, 5)
testDriver.test(() => { return calg.ne(z, w) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_14', false)

w = calg.num(1, 2, 5)
testDriver.test(() => { return calg.ne(z, w) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_15', false)

// isZero
testDriver.test(() => { return calg.isZero(calg.num(0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_16', false)
testDriver.test(() => { return calg.isZero(calg.inum(0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_17', false)
testDriver.test(() => { return calg.isZero(calg.num(1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_18', false)
testDriver.test(() => { return calg.isZero(calg.inum(1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_19', false)
testDriver.test(() => { return calg.isZero(calg.num(-1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_20', false)
testDriver.test(() => { return calg.isZero(calg.inum(-1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_21', false)

// isInteger (since v1.1.0)
testDriver.test(() => { return calg.isInteger(calg.num(0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_22', false)
testDriver.test(() => { return calg.isInteger(calg.inum(0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_23', false)
testDriver.test(() => { return calg.isInteger(calg.num(1, 2)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_24', false)
testDriver.test(() => { return calg.isInteger(calg.inum(1, 2)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_25', false)
testDriver.test(() => { return calg.isInteger(calg.num(6, 3)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_26', false)
testDriver.test(() => { return calg.isInteger(calg.inum(6, 3)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_27', false)
testDriver.test(() => { return calg.isInteger(calg.num(6, 3, 2)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_28', false)
testDriver.test(() => { return calg.isInteger(calg.inum(6, 3, 2)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra-example0_29', false)

// Addition
z = calg.num(1, 2, 5)
w = calg.inum(1, 2)
// new object is generated
v = calg.add(z, w)
testDriver.test(() => { return v.toString() }, '(1 / 2)sqrt(5) + i(1 / 2)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_30', false)

// In-place addition
z = calg.num(1, 2, 5)
w = calg.inum(1, 2)
// new object is not generated
z = calg.iadd(z, w)
testDriver.test(() => { return z.toString() }, '(1 / 2)sqrt(5) + i(1 / 2)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_31', false)

// Subtraction
z = calg.num(1, 2, 5)
w = calg.inum(1, 2)
// new object is generated
v = calg.sub(z, w)
testDriver.test(() => { return v.toString() }, '(1 / 2)sqrt(5) + i(-1 / 2)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_32', false)

// In-place subtraction
z = calg.num(1, 2, 5)
w = calg.inum(1, 2)
// new object is not generated
z = calg.isub(z, w)
testDriver.test(() => { return z.toString() }, '(1 / 2)sqrt(5) + i(-1 / 2)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_33', false)

// Maltiplication
z = calg.inum(1, 2, 5)
w = calg.inum(1, 2)
// new object is generated
v = calg.mul(z, w)
testDriver.test(() => { return v.toString() }, '-(1 / 4)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_34', false)

// In-place multiplication
z = calg.inum(1, 2, 5)
w = calg.inum(1, 2)
// new object is not generated
z = calg.imul(z, w)
testDriver.test(() => { return z.toString() }, '-(1 / 4)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_35', false)

// Division
z = calg.inum(1, 2, 5)
w = calg.inum(1, 2)
// new object is generated
v = calg.div(z, w)
testDriver.test(() => { return v.toString() }, 'sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_36', false)

// In-place division
z = calg.inum(1, 2, 5)
w = calg.inum(1, 2)
// new object is not generated
z = calg.idiv(z, w)
testDriver.test(() => { return z.toString() }, 'sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_37', false)

// Multiplication by -1
z = calg.inum(1, 2, 5)
// new object is generated
w = calg.neg(z)
testDriver.test(() => { return w.toString() }, 'i(-(1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_38', false)

// In-place multiplication by -1
z = calg.inum(1, 2, 5)
// new object is not generated
z = calg.ineg(z)
testDriver.test(() => { return z.toString() }, 'i(-(1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_39', false)

// Complex conjugate
z = calg.num(1, 2, 5)
w = calg.inum(1, 2, 5)
// new object is generated
v = calg.cjg(z)
testDriver.test(() => { return v.toString() }, '(1 / 2)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_40', false)
v = calg.cjg(w)
testDriver.test(() => { return v.toString() }, 'i(-(1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_41', false)

// In-place evaluation of the complex conjugate
z = calg.num(1, 2, 5)
w = calg.inum(1, 2, 5)
// new object is not generated
z = calg.icjg(z)
testDriver.test(() => { return z.toString() }, '(1 / 2)sqrt(5)', 'src/complex-algebra.mjs~ComplexAlgebra-example0_42', false)
w = calg.icjg(w)
testDriver.test(() => { return w.toString() }, 'i(-(1 / 2)sqrt(5))', 'src/complex-algebra.mjs~ComplexAlgebra-example0_43', false)

// Square of the absolute value
z = calg.iadd(calg.num(3), calg.inum(4))
a = calg.abs2(z)
testDriver.test(() => { return a.toString() }, '25', 'src/complex-algebra.mjs~ComplexAlgebra-example0_44', false)
// return value is not a complex number (but a real number)
testDriver.test(() => { return a.re }, undefined, 'src/complex-algebra.mjs~ComplexAlgebra-example0_45', false)
testDriver.test(() => { return a.im }, undefined, 'src/complex-algebra.mjs~ComplexAlgebra-example0_46', false)

// JSON (stringify and parse)
z = calg.iadd(calg.num(1, 2, 5), calg.inum(-1, 2, 7))
let str = JSON.stringify(z)
w = JSON.parse(str, calg.reviver)
testDriver.test(() => { return calg.eq(z, w) }, true, 'src/complex-algebra.mjs~ComplexAlgebra-example0_47', false)
