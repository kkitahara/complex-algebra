import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

testDriver.test(() => { return calg.isExact() }, false, 'src/complex-algebra.mjs~ComplexAlgebra#isExact-example1_0', false)
