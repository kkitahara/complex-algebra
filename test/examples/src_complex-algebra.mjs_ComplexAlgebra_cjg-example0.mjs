import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.iadd(calg.num(1), calg.inum(-1))

let w = calg.cjg(z)
testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cjg-example0_0', false)
testDriver.test(() => { return w !== z }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cjg-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(1, -1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cjg-example0_2', false)
testDriver.test(() => { return calg.eq(w, new C(1, 1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cjg-example0_3', false)
