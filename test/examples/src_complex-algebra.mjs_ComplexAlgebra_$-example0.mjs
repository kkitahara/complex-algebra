import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.$(1, 2)
testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_0', false)
testDriver.test(() => { return ralg.eq(z.re, 1) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_1', false)
testDriver.test(() => { return ralg.eq(z.im, 2) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_2', false)

z = calg.$()
testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_3', false)
testDriver.test(() => { return ralg.eq(z.re, 0) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_4', false)
testDriver.test(() => { return ralg.eq(z.im, 0) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#$-example0_5', false)
