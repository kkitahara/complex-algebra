import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.iadd(calg.num(1), calg.inum(-1))

let w = calg.abs(z)
testDriver.test(() => { return w instanceof C }, false, 'src/complex-algebra.mjs~ComplexAlgebra#abs-example1_0', false)
testDriver.test(() => { return typeof w === 'number' }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs-example1_1', false)
testDriver.test(() => { return calg.eq(z, new C(1, -1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs-example1_2', false)
testDriver.test(() => { return ralg.eq(w, Math.sqrt(2)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs-example1_3', false)
