import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1, 2, 5)
let w = calg.num(-1, 2, -5)
let v = z

// GOOD-PRACTICE!
z = calg.imul(z, w)
testDriver.test(() => { return z === v }, true, 'src/complex-algebra.mjs~ComplexAlgebra#imul-example0_0', false)
testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#imul-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(0, ralg.num(-5, 4))) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#imul-example0_2', false)
