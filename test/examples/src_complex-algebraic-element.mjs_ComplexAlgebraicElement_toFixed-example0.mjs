import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
let b = ralg.iadd(ralg.num(-1), a)
let z = new C(a, b)
testDriver.test(() => { return z.toFixed(3) }, '-1.618 + i(-2.618)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_0', false)

z = new C(0, b)
testDriver.test(() => { return z.toFixed(3) }, 'i(-2.618)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_1', false)

z = new C(ralg.num(0), b)
testDriver.test(() => { return z.toFixed(3) }, 'i(-2.618)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_2', false)

z = new C(b, ralg.num(0))
testDriver.test(() => { return z.toFixed(3) }, '-2.618', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_3', false)

z = new C(1.3, 3.2)
testDriver.test(() => { return z.toFixed(3) }, '1.300 + i(3.200)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_4', false)

z = new C(0, 3.2)
testDriver.test(() => { return z.toFixed(3) }, 'i(3.200)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_5', false)

z = new C(0, 0)
testDriver.test(() => { return z.toFixed(3) }, '0.000', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toFixed-example0_6', false)
