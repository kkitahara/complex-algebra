import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1)
let w = calg.copy(z)

testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_0', false)
testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_1', false)
testDriver.test(() => { return ralg.eq(z.re, w.re) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_2', false)
testDriver.test(() => { return ralg.eq(z.im, w.im) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_3', false)
testDriver.test(() => { return z !== w }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_4', false)
testDriver.test(() => { return z.re !== w.re }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_5', false)
testDriver.test(() => { return z.im !== w.im }, true, 'src/complex-algebra.mjs~ComplexAlgebra#copy-example0_6', false)
