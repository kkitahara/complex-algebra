import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1, 2, 5)
let w = calg.num(-1, 2, -5)

let v = calg.mul(z, w)
testDriver.test(() => { return v instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#mul-example0_0', false)
testDriver.test(() => { return v !== z }, true, 'src/complex-algebra.mjs~ComplexAlgebra#mul-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(ralg.num(1, 2, 5), 0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#mul-example0_2', false)
testDriver.test(() => { return calg.eq(v, new C(0, ralg.num(-5, 4))) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#mul-example0_3', false)
