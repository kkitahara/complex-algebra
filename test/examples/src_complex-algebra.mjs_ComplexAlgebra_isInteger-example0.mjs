import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

testDriver.test(() => { return calg.isInteger(calg.num(0, 2, 5)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#isInteger-example0_0', false)
testDriver.test(() => { return calg.isInteger(calg.num(1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#isInteger-example0_1', false)
testDriver.test(() => { return calg.isInteger(calg.inum(1, 2, 5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#isInteger-example0_2', false)
testDriver.test(() => { return calg.isInteger(calg.num(4, 2, 1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#isInteger-example0_3', false)
testDriver.test(() => { return calg.isInteger(calg.inum(4, 2, 1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#isInteger-example0_4', false)
