import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.iadd(calg.num(1), calg.inum(-1))

let w = calg.abs2(z)
testDriver.test(() => { return w instanceof C }, false, 'src/complex-algebra.mjs~ComplexAlgebra#abs2-example0_0', false)
testDriver.test(() => { return w instanceof P }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs2-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(1, -1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs2-example0_2', false)
testDriver.test(() => { return ralg.eq(w, ralg.num(2)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#abs2-example0_3', false)
