import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1)
let w = z

testDriver.test(() => { return w === calg.cast(z) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_0', false)
testDriver.test(() => { return calg.cast(1) instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_1', false)
testDriver.test(() => { return w === calg.cast(1) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_2', false)
testDriver.test(() => { return calg.eq(z, calg.cast(1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_3', false)

testDriver.test(() => { return calg.eq(calg.cast({ re: 2, im: 1 }), new C(2, 1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_4', false)

z = new C(1, 2)
testDriver.test(() => { return ralg.cast(z.re) === z.re }, false, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_5', false)
w = calg.cast(z)
testDriver.test(() => { return w === z }, false, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_6', false)
testDriver.test(() => { return ralg.cast(w.re) === w.re }, true, 'src/complex-algebra.mjs~ComplexAlgebra#cast-example0_7', false)
