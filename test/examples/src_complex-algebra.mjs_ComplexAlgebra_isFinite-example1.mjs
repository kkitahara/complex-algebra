import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let a = 1e999
testDriver.test(() => { return calg.isFinite(calg.num(1, 2, 5)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#isFinite-example1_0', false)
testDriver.test(() => { return calg.isFinite(calg.num(a, 2, -5)) }, false, 'src/complex-algebra.mjs~ComplexAlgebra#isFinite-example1_1', false)
