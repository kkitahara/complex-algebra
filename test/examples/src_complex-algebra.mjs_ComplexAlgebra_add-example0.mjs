import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.num(1)
let w = calg.num(-1, 1, -1)

let v = calg.add(z, w)
testDriver.test(() => { return v instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#add-example0_0', false)
testDriver.test(() => { return v !== z }, true, 'src/complex-algebra.mjs~ComplexAlgebra#add-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(1, 0)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#add-example0_2', false)
testDriver.test(() => { return calg.eq(v, new C(1, -1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#add-example0_3', false)
