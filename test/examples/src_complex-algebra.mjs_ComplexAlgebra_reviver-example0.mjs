import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.iadd(calg.num(1, 2, 5), calg.inum(-1, 2, 7))
let str = JSON.stringify(z)
let w = JSON.parse(str, calg.reviver)
testDriver.test(() => { return calg.eq(z, w) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#reviver-example0_0', false)
