import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
import bigInt from 'big-integer'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let a = ralg.num(1, 2, 5)
let b = ralg.num(0, 2, 5)
let z = calg.num(1, 2, 5)
let w = calg.num(1, 2, -5)

testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_0', false)
testDriver.test(() => { return ralg.eq(z.re, a) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_1', false)
testDriver.test(() => { return ralg.eq(z.im, b) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_2', false)

testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_3', false)
testDriver.test(() => { return ralg.eq(w.re, b) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_4', false)
testDriver.test(() => { return ralg.eq(w.im, a) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_5', false)

w = calg.num(1, 2, bigInt(-5))
testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_6', false)
testDriver.test(() => { return ralg.eq(w.re, b) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_7', false)
testDriver.test(() => { return ralg.eq(w.im, a) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_8', false)

w = calg.num()
testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_9', false)
testDriver.test(() => { return ralg.eq(w.re, 0) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_10', false)
testDriver.test(() => { return ralg.eq(w.im, 0) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#num-example0_11', false)
