import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C } from '../../src/index.mjs'
let ralg = new RealAlgebra()

testDriver.test(() => { return new C(1, 0).toString() }, '1', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_0', false)
testDriver.test(() => { return new C(0, -1).toString() }, 'i(-1)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_1', false)
testDriver.test(() => { return new C(-1, -1).toString() }, '-1 + i(-1)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_2', false)

let a = ralg.iadd(ralg.num(-1, 2), ralg.num(-1, 2, 5))
let b = ralg.iadd(ralg.num(-1), a)
let z = new C(a, b)
testDriver.test(() => { return z.toString() }, '-1 / 2 - (1 / 2)sqrt(5) + i(-3 / 2 - (1 / 2)sqrt(5))', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_3', false)

testDriver.test(() => { return new C(-3, -5).toString(2) }, '-11 + i(-101)', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_4', false)

testDriver.test(() => { return new C(0, 0).toString(2) }, '0', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#toString-example0_5', false)
