import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ComplexAlgebraicElement as C } from '../../src/index.mjs'

let a = 1 / 2 * Math.sqrt(5)
let b = a - 1
let z = new C(a, b)
let s = JSON.stringify(z)

let w = JSON.parse(s, C.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_0', false)
testDriver.test(() => { return w instanceof C }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_1', false)
testDriver.test(() => { return w !== z }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_2', false)
testDriver.test(() => { return z.re === w.re }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_3', false)
testDriver.test(() => { return z.im === w.im }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_4', false)

let s2 = s.replace('1.0.0', '0.0.0')
testDriver.test(() => { return JSON.parse(s2, C.reviver) }, Error, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement.reviver-example0_5', false)
