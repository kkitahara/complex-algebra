import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
import { ComplexAlgebraicElement as C, ComplexAlgebra }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let calg = new ComplexAlgebra(ralg)

let z = calg.iadd(calg.num(1), calg.inum(-1))
let w = z

// GOOD-PRACTICE!
z = calg.icjg(z)
testDriver.test(() => { return z === w }, true, 'src/complex-algebra.mjs~ComplexAlgebra#icjg-example0_0', false)
testDriver.test(() => { return z instanceof C }, true, 'src/complex-algebra.mjs~ComplexAlgebra#icjg-example0_1', false)
testDriver.test(() => { return calg.eq(z, new C(1, 1)) }, true, 'src/complex-algebra.mjs~ComplexAlgebra#icjg-example0_2', false)
