import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ComplexAlgebraicElement as C } from '../../src/index.mjs'

let a = new C(1, 2)
testDriver.test(() => { return a.re === 1 }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#constructor-example0_0', false)
testDriver.test(() => { return a.im === 2 }, true, 'src/complex-algebraic-element.mjs~ComplexAlgebraicElement#constructor-example0_1', false)
